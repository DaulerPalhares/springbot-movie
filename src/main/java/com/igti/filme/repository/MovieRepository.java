package com.igti.filme.repository;

import com.igti.filme.entitiy.Movie;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface MovieRepository extends MongoRepository<Movie, String>{

}