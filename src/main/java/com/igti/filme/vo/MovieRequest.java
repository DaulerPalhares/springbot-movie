package com.igti.filme.vo;

import lombok.Data;

@Data
public class MovieRequest{
    private String title;
    private String director;
    private String genere;
    private int year;
}