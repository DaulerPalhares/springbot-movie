package com.igti.filme.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.igti.filme.entitiy.Movie;
import com.igti.filme.repository.MovieRepository;
import com.igti.filme.vo.MovieRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/movies")
public class FilmeController{

    @Autowired
    private MovieRepository movieRepository;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Movie>> getAllMovies() {
        
        List<Movie> movies = movieRepository.findAll();

        return new ResponseEntity<>(movies,HttpStatus.OK);
    }

    @RequestMapping(path = "{id}" , method = RequestMethod.GET)
    public ResponseEntity<Movie> getMovieById(@PathVariable String id){
        Optional<Movie> movieOptional = movieRepository.findById(id);

        if(!movieOptional.isPresent()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(movieOptional.get(),HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> addMovie(@RequestBody @Valid MovieRequest request){
        Movie movie = new Movie();
        movie.setTitle(request.getTitle());
        movie.setDirector(request.getDirector());
        movie.setGenere(request.getGenere());
        movie.setYear(request.getYear());

        movieRepository.save(movie);

        return new ResponseEntity<>(HttpStatus.CREATED);

    }

    @RequestMapping(path = "{id}" , method = RequestMethod.PUT)
    public ResponseEntity<Void> changeMovieInformation(@PathVariable String id, @RequestBody @Valid MovieRequest request){
        Optional<Movie> movieOptional = movieRepository.findById(id);

        if(!movieOptional.isPresent()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        movieOptional.get().setTitle(request.getTitle());
        movieOptional.get().setDirector(request.getDirector());
        movieOptional.get().setGenere(request.getGenere());
        movieOptional.get().setYear(request.getYear());

        movieRepository.save(movieOptional.get());

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(path = "{id}" , method = RequestMethod.PATCH)
    public ResponseEntity<Void> changeInformation(@PathVariable String id, @RequestBody @Valid MovieRequest request){
        Optional<Movie> movieOptional = movieRepository.findById(id);

        if(!movieOptional.isPresent()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if(request.getTitle() != null){
            movieOptional.get().setTitle(request.getTitle());
        }
        if(request.getDirector() != null){        
            movieOptional.get().setDirector(request.getDirector());
        }
        if(request.getGenere() != null){        
            movieOptional.get().setGenere(request.getGenere());
        }
        if(request.getYear() > 1700){
            movieOptional.get().setYear(request.getYear());
        }
        movieRepository.save(movieOptional.get());

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(path = "{id}" , method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteMovie(@PathVariable String id){
        Optional<Movie> movieOptional = movieRepository.findById(id);

        if(!movieOptional.isPresent()){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        movieRepository.delete(movieOptional.get());

        return new ResponseEntity<>(HttpStatus.OK);
    }

}